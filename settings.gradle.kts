
pluginManagement {
    repositories{
        gradlePluginPortal()
    }
}

rootProject.name = "rell-gradle-plugin"
include("plugin")
