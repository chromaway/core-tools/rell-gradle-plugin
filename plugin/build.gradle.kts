import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import groovy.namespace.QName
import groovy.util.Node
import groovy.util.NodeList
import org.gradle.api.tasks.testing.logging.TestExceptionFormat

plugins {
    `java-gradle-plugin`
    id("org.jetbrains.kotlin.jvm") version "2.0.0"
    id("com.github.johnrengelman.shadow") version "8.1.0"
    `maven-publish`
    id("jacoco-report-aggregation")
    jacoco
}

group = "com.chromia.rell.gradle"

tasks.named("shadowJar", ShadowJar::class.java) {
}

tasks.shadowJar {
    archiveClassifier.set("")
    mergeServiceFiles()
    isZip64 = true
}

tasks.withType<AbstractArchiveTask> {
    isReproducibleFileOrder = true
    isPreserveFileTimestamps = false
}

publishing {
    repositories {
        maven {
            name = ("GitLab")
            url = uri("https://gitlab.com/api/v4/projects/37675750/packages/maven")
            credentials(HttpHeaderCredentials::class.java) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/64941451/packages/maven") { name = "Chromia CLI Tools" }
    maven("https://gitlab.com/api/v4/projects/37660703/packages/maven") { name = "Rell Codegen" }
    maven("https://gitlab.com/api/v4/projects/32294340/packages/maven") { name = "Postchain" }
    maven("https://gitlab.com/api/v4/projects/32802097/packages/maven") { name = "Rell" }
    maven("https://gitlab.com/api/v4/projects/50818999/packages/maven") { name = "Chromia Parent" }
    maven("https://gitlab.com/api/v4/projects/33481005/packages/maven") { name = "Postchain Chromia" }
    maven("https://gitlab.com/api/v4/projects/46288950/packages/maven") { name = "Postchain Client" }
    maven("https://gitlab.com/api/v4/projects/55009888/packages/maven") { name = "Rell Dokka" }
    maven("https://jcenter.bintray.com")
    maven("https://maven.emrld.io")
}

val functionalTestImplementation by configurations.creating {
    extendsFrom(configurations.testImplementation.get())
}

dependencies {
    shadow(gradleApi())

    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation(enforcedPlatform("com.fasterxml.jackson:jackson-bom:2.15.3"))

    // Use the Kotlin JDK 8 standard library.
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Use the Kotlin test library.
    testImplementation("org.jetbrains.kotlin:kotlin-test")

    // Use the Kotlin JUnit integration.
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testImplementation("com.willowtreeapps.assertk:assertk:0.28.1")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.8.1")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5:1.9.21")

    implementation("com.chromia.cli.tools:chromia-build-tools:${libs.versions.chromia.cli.tools.get()}")
    implementation("net.postchain.rell:rell-api-gtx:${libs.versions.rell.version.get()}")
    implementation("net.postchain.rell:codegen:${libs.versions.rell.codegen.get()}")
    implementation("net.postchain.rell:codegen-kotlin:${libs.versions.rell.codegen.get()}")
    implementation("com.chromia.rell.dokka:rell-dokka-plugin:${libs.versions.rell.dokka.get()}")
}

tasks {
    withType<Test> {
        testLogging {
            events("failed")
            exceptionFormat = TestExceptionFormat.FULL
        }
    }
}
testing {
    suites {
        val test by getting(JvmTestSuite::class) {
            useJUnitJupiter()
        }

        register<JvmTestSuite>("functionalTest") {
            dependencies {
                implementation(project())
            }
            testType = TestSuiteType.FUNCTIONAL_TEST
            targets {
                all {
                    testTask.configure {
                        shouldRunAfter(test)
                    }
                }
            }
        }
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_21
    targetCompatibility = JavaVersion.VERSION_21
}
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        jvmTarget = "21"
    }
}
tasks.check {
    dependsOn(testing.suites.named("functionalTest"))
    dependsOn(tasks.testCodeCoverageReport)
}
tasks.jacocoTestReport {
    dependsOn(testing.suites.named("functionalTest"))
    executionData(layout.buildDirectory.file("jacoco/functionalTest.exec"))
}
tasks.testCodeCoverageReport {
    dependsOn(testing.suites.named("functionalTest"))
    executionData(layout.buildDirectory.file("jacoco/functionalTest.exec"))
}

// Plugin publication section
gradlePlugin {
    testSourceSets(sourceSets.named("functionalTest").get())
    plugins {
        create("rellPlugin") {
            id = "com.chromia.rell"
            implementationClass = "com.chromia.rell.gradle.plugin.RellGradlePlugin"
        }
    }
}

tasks.withType<GenerateModuleMetadata> {
    enabled = false
}


// Remove all dependencies from output-pom. This can be done since we are shading all deps
publishing {
    publications {
        create<MavenPublication>("pluginMaven") {
            pom.withXml {
                asNode().get("dependencies")
                    .let { it as NodeList }
                    .filterIsInstance<Node>()
                    .flatMap { it.children() }
                    .filterIsInstance<Node>()
                    .filter { (it.name() as QName).localPart == "dependency" }
                    .forEach { it.parent().remove(it) }
            }
        }
    }
}
