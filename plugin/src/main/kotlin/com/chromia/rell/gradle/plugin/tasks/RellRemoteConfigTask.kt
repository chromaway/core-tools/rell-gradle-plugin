package com.chromia.rell.gradle.plugin.tasks

import com.chromia.build.tools.lib.GitRepositoryCloner
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.file.Directory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectories
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

abstract class RellRemoteConfigTask : DefaultTask() {
    @get:Input
    abstract val repository: Property<String>

    @get:Optional
    @get:Input
    abstract val configPath: Property<String>

    @get:Optional
    @get:Input
    abstract val tagOrBranch: Property<String>

    @get:OutputDirectory
    val output = project.objects.directoryProperty().convention(project.layout.buildDirectory.dir("rell-repo"))

    @TaskAction
    fun generateCode() {
        output.get().asFile.deleteRecursively()
        GitRepositoryCloner().clone(repository.get(), output.get().asFile.toPath(), tagOrBranch.orNull)
        if (!output.get().file(configPath).get().asFile.exists()) throw GradleException("Repository $repository does not hav a chromia.yml at $configPath")

    }
}
