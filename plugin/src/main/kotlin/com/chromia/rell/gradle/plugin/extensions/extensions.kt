package com.chromia.rell.gradle.plugin.extensions

import org.gradle.api.Action
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Nested
import org.gradle.api.tasks.Optional


abstract class ChromiaExtension {
    abstract val config: RegularFileProperty

    @get:Nested
    abstract val remote: ChromiaRemoteRepositoryExtension

    @get:Nested
    abstract val build: ChromiaBuildExtension

    @get:Nested
    abstract val install: ChromiaLibsExtension

    @get:Nested
    abstract val client: ChromiaGeneratedStubsExtension

    @get:Nested
    abstract val test: ChromiaTestExtension

    fun remote(action: Action<in ChromiaRemoteRepositoryExtension>) {
        action.execute(remote)
    }

    fun build(action: Action<in ChromiaBuildExtension>) {
        action.execute(build)
    }

    fun install(action: Action<in ChromiaLibsExtension>) {
        action.execute(install)
    }

    fun client(action: Action<in ChromiaGeneratedStubsExtension>) {
        action.execute(client)
    }

    fun test(action: Action<in ChromiaTestExtension>) {
        action.execute(test)
    }
}

interface ChromiaRemoteRepositoryExtension {
    val repositoryUrl: Property<String>
    val tagOrBranch: Property<String>
}

interface ChromiaBuildExtension {
    val blockchains: ListProperty<String>
}

interface ChromiaLibsExtension {
    val libs: ListProperty<String>
}

interface ChromiaGeneratedStubsExtension {
    val modules: ListProperty<String>
    val packageName: Property<String>
}

interface ChromiaTestExtension {
    val useDb: Property<Boolean>
    val blockchains: ListProperty<String>
    val modules: ListProperty<String>
    val testReport: Property<Boolean>
    val failOnError: Property<Boolean>
}
