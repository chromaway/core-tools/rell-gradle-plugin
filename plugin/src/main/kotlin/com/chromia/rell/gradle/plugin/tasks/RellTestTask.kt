package com.chromia.rell.gradle.plugin.tasks


import com.chromia.api.filterBlockchains
import com.chromia.build.tools.iccf.AbstractTestIccfGtxModule
import com.chromia.build.tools.test.xmlTestReport
import com.chromia.cli.model.BlockchainModel
import com.chromia.cli.model.ChromiaModel
import net.postchain.core.TxEContext
import net.postchain.gtv.Gtv
import net.postchain.gtx.GTXOperation
import net.postchain.rell.api.base.RellApiCompile
import net.postchain.rell.api.gtx.RellApiRunTests
import net.postchain.rell.base.utils.UnitTestCase
import net.postchain.rell.base.utils.UnitTestCaseResult
import net.postchain.rell.base.utils.UnitTestResult
import org.gradle.api.GradleException
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import java.nio.file.Files

abstract class RellTestTask : ChromiaTask() {

    @get:Optional
    @get:Input
    abstract val blockchains: ListProperty<String>

    @get:Input
    abstract val failOnError: Property<Boolean>

    @get:Input
    abstract val useDb: Property<Boolean>

    @get:Optional
    @get:Input
    abstract val modules: ListProperty<String>

    @get:Optional
    @get:Input
    abstract val tests: ListProperty<String>

    @get:Input
    abstract val testReport: Property<Boolean>

    override val model: ChromiaModel
        get() = super.model.filterBlockchains(blockchains.orNull ?: listOf())
            .filterTestableBlockchains()

    @get:InputDirectory
    val source by lazy { model.compile.source }

    @get:OutputDirectory
    val target by lazy { project.layout.buildDirectory.file("reports/rell") }

    @TaskAction
    fun runUnitTests() {
        if (blockchains.isPresent && blockchains.get().isNotEmpty()) return
        val modules = modules.get().takeIf { it.isNotEmpty() } ?: model.test.modules
        val moduleArgs = model.test.moduleArgs
        val config = createTestConfig(moduleArgs)
        val res = RellApiRunTests.runTests(config, source.toFile(), listOf(), modules)
        Files.writeString(target.get().asFile.toPath().resolve("rell-unit-tests.xml"),  res.xmlTestReport("rell"))
    }

    @TaskAction
    fun runBlockchainTests() {
        model.blockchains.forEach { (name, bcModel) ->
            if (modules.isPresent && !bcModel.test.modules.containsAll(modules.get())) {
                throw GradleException("Configured modules are not present in blockchain $name")
            }
            val testModules = modules.get().takeIf { it.isNotEmpty() } ?: bcModel.test.modules
            val moduleArgs = bcModel.testModuleArgs()
            val additionalModules = bcModel.config["gtx"]?.get("modules")?.asArray()?.map { it.asString() }
            val config = createTestConfig(moduleArgs, additionalModules, appModuleInTestsError = true)
            val appModules: List<String>? = bcModel.module?.let { listOf(it) }

            val res = RellApiRunTests.runTests(config, source.toFile(), appModules, testModules)
            Files.writeString(target.get().asFile.toPath().resolve("rell-blockchain-$name-tests.xml"),  res.xmlTestReport("blockchain-$name"))
        }
    }

    private fun ChromiaModel.filterTestableBlockchains() =
        copy(blockchains = blockchains.filter { it.value.test.modules.isNotEmpty() })

    private fun BlockchainModel.testModuleArgs() = (moduleArgs.asSequence() + test.moduleArgs.asSequence())
        .groupBy({ it.key }, { it.value })
        .mapValues { (_, values) ->
            values.flatMap { map -> map.entries }.associate(Map.Entry<String, Gtv>::toPair)
        }

    private fun createTestConfig(testModuleArgs: Map<String, Map<String, Gtv>>, additionalGtxModules: List<String>? = null,
                                 appModuleInTestsError: Boolean = false): RellApiRunTests.Config {
        val compileConf = RellApiCompile.Config.Builder()
            .moduleArgs(testModuleArgs)
            .cliEnv(env)
            .includeTestSubModules(true)
            .appModuleInTestsError(appModuleInTestsError)
            .addWhitelistedGTXModules(additionalGtxModules)
            .moduleArgsMissingError(true)
            .mountConflictError(true)
            .version(model.compile.langVersion)
            .quiet(model.compile.quiet)
            .build()

        return RellApiRunTests.Config.Builder()
            .compileConfig(compileConf)
            //.testPatterns(tests.orNull?.takeIf { it.isNotEmpty() })
            .databaseUrl(useDb.get().takeIf { it }?.let { "${model.databaseUrl}&currentSchema=${model.databaseSchema}_tests" })
            .stopOnError(failOnError.get() ?: model.test.failOnError)
            .sqlErrorLog(model.logSqlErrors)
            .logPrinter(logger::info)
            .outPrinter(logger::info)
            .printTestCases(false)
            .onTestCaseStart { case -> case.print() }
            .onTestCaseFinished { res -> res.print() }
            .sqlLog(false)
            .build()
    }

    private val whitelistedTestGtxModuleMap: Map<String, String> = mapOf(
        "net.postchain.d1.iccf.IccfGTXModule" to DummyIccfGtxModule::class.qualifiedName!!
    )
    fun RellApiCompile.Config.Builder.addWhitelistedGTXModules(gtxModules: List<String>?) = apply {
        gtxModules?.let { m ->
            additionalGtxModules(m.mapNotNull { whitelistedTestGtxModuleMap[it] })
        }
    }

    private fun UnitTestCase.print() {
        logger.info("TEST: $name")
    }

    private fun UnitTestCaseResult.print() {
        if (res.isOk) {
            logger.info("${res}: $case (${UnitTestResult.durationToString(res.duration)})")
        } else {
            logger.warn("${res}: $case (${UnitTestResult.durationToString(res.duration)})")
        }
    }

}
class DummyIccfGtxModule: AbstractTestIccfGtxModule<DummyIccfGtxModule.Conf>(
    conf = Conf(),
    {_,data -> object : GTXOperation(data) {
        override fun checkCorrectness() { }
        override fun apply(ctx: TxEContext)= true
    } }
) {
    class Conf
}