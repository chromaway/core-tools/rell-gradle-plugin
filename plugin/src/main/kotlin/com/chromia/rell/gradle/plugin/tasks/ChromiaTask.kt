package com.chromia.rell.gradle.plugin.tasks

import com.chromia.cli.model.parseModel
import net.postchain.rell.api.base.RellCliEnv
import org.gradle.api.DefaultTask
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Internal

abstract class ChromiaTask: DefaultTask() {

    @get:InputFile
    abstract val config: RegularFileProperty

    @get:Internal
    open val model by lazy { parseModel(config.get().asFile.toPath()) }

    @Internal
    val env = object: RellCliEnv {
        override fun print(msg: String) {
            logger.info(msg)
        }

        override fun error(msg: String) {
            logger.error(msg)
        }
    }
}
