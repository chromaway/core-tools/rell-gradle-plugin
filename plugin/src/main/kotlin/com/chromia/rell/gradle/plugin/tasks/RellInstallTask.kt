package com.chromia.rell.gradle.plugin.tasks


import com.chromia.api.ChromiaLibrariesApi
import com.chromia.api.filterLibraries
import com.chromia.build.tools.lib.GitRepositoryCloner
import com.chromia.cli.model.ChromiaModel
import org.gradle.api.provider.ListProperty
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectories
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.StopExecutionException
import org.gradle.api.tasks.TaskAction

abstract class RellInstallTask : ChromiaTask() {

    @get:Optional
    @get:Input
    abstract val libs: ListProperty<String>

    override val model: ChromiaModel
        get() = super.model.filterLibraries(libs.get().takeIf { it.isNotEmpty() })

    @get:OutputDirectories
    val target by lazy { model.libs.map { model.compile.source.resolve("lib/${it.key}") } }

    @get:OutputDirectory
    val tmp by lazy { model.compile.target.resolve(".tmp") }

    @TaskAction
    fun build() {
        //if (model.libs.isEmpty()) throw StopExecutionException()
        ChromiaLibrariesApi.install(env, model, GitRepositoryCloner())
    }
}
