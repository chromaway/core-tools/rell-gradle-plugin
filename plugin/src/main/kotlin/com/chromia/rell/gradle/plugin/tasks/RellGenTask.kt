package com.chromia.rell.gradle.plugin.tasks

import net.postchain.rell.codegen.CodeGenerator
import net.postchain.rell.codegen.document.DocumentSaver
import net.postchain.rell.codegen.kotlin.KotlinCodeGeneratorConfig
import net.postchain.rell.codegen.kotlin.KotlinDocumentFactory
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

abstract class RellGenTask : ChromiaTask() {
    @get:Input
    abstract val packageName: Property<String>

    @get:Optional
    @get:Input
    abstract val modules: ListProperty<String>

    @get:OutputDirectory
    val output = project.layout.buildDirectory.dir("generated-sources/client")

    @TaskAction
    fun generateCode() {
        logger.error("Source files: ${model.compile.source}")
        val config = object : KotlinCodeGeneratorConfig {
            override fun packageName() = packageName.get()
        }
        val generator = CodeGenerator(KotlinDocumentFactory(config), config)
        val sections = generator.createSections(model.compile.source.toFile(), modules.orNull)
        val documents = generator.constructDocuments(sections)
        DocumentSaver(output.get().asFile).saveDocuments(documents)
    }
}
