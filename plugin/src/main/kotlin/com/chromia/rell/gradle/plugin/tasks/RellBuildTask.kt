package com.chromia.rell.gradle.plugin.tasks


import com.chromia.api.ChromiaCompileApi
import com.chromia.api.filterBlockchains
import com.chromia.cli.model.ChromiaModel
import org.gradle.api.provider.ListProperty
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputFiles
import org.gradle.api.tasks.TaskAction

abstract class RellBuildTask : ChromiaTask() {

    @get:Optional
    @get:Input
    abstract val blockchains: ListProperty<String>

    override val model: ChromiaModel
        get() = super.model.filterBlockchains(blockchains.orNull ?: listOf())

    @get:InputDirectory
    val source by lazy { model.compile.source }

    @get:OutputFiles
    val target by lazy { model.blockchains.map { model.compile.target.resolve("${it.key}.xml").toFile() } }

    @TaskAction
    fun build() {
        ChromiaCompileApi.build(env, model).onEach { it.save(model.compile.target) }
    }
}
