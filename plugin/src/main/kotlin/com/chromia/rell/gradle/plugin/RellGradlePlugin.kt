package com.chromia.rell.gradle.plugin

import com.chromia.rell.gradle.plugin.extensions.ChromiaExtension
import com.chromia.rell.gradle.plugin.tasks.ChromiaTask
import com.chromia.rell.gradle.plugin.tasks.RellBuildTask
import com.chromia.rell.gradle.plugin.tasks.RellDocsTask
import com.chromia.rell.gradle.plugin.tasks.RellGenTask
import com.chromia.rell.gradle.plugin.tasks.RellInstallTask
import com.chromia.rell.gradle.plugin.tasks.RellRemoteConfigTask
import com.chromia.rell.gradle.plugin.tasks.RellTestTask
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.SourceSetContainer
import java.io.File
import org.gradle.api.file.SourceDirectorySet

class RellGradlePlugin : Plugin<Project> {

    override fun apply(project: Project) {
        val extension = project.extensions.create("chromia", ChromiaExtension::class.java).apply {
            config.convention { File(project.projectDir, "chromia.yml") }
            test.failOnError.convention(true)
            test.useDb.convention(true)
            test.testReport.convention(true)
        }

        val externalRepoTask = project.tasks.register("rellRemoteRepo", RellRemoteConfigTask::class.java) { task ->
            task.group = "Chromia"
            task.description = "Use remote rell repository"
            task.onlyIf { extension.remote.repositoryUrl.isPresent }
            task.configPath.set(extension.config.get().asFile.name)
            task.repository.set(extension.remote.repositoryUrl)
            task.tagOrBranch.set(extension.remote.tagOrBranch)
        }

        val installTask = project.tasks.register("rellInstall", RellInstallTask::class.java) { task ->
            task.group = "Chromia"
            task.description = "Install Rell Libraries"
            task.config.set(extension.config)
            task.libs.set(extension.install.libs)
            task.onlyIf("No libraries to install") { task.model.libs.isNotEmpty() }
            task.setChromiaConfig(externalRepoTask.get())
            task.dependsOn(externalRepoTask)
        }
        project.tasks.register("rellBuild", RellBuildTask::class.java) { task ->
            task.group = "Chromia"
            task.description = "Build dapp into blockchain configuration files"
            task.config.set(extension.config)
            task.blockchains.set(extension.build.blockchains)
            task.setChromiaConfig(externalRepoTask.get())
            task.dependsOn(installTask, externalRepoTask)
        }
        project.tasks.register("rellTest", RellTestTask::class.java) { task ->
            task.group = "Chromia"
            task.description = "Run Rell Test Suite"
            task.config.set(extension.config)
            val testExtension = extension.test
            task.blockchains.set(testExtension.blockchains)
            task.failOnError.set(testExtension.failOnError)
            task.testReport.set(testExtension.testReport)
            task.tests.set(project.providers.gradleProperty("rellTests").map { it.split(",") })
            task.modules.set(testExtension.modules)
            task.useDb.set(testExtension.useDb)
            task.setChromiaConfig(externalRepoTask.get())
            task.dependsOn(installTask, externalRepoTask)
        }
        project.tasks.register("rellDocs", RellDocsTask::class.java) { task ->
            task.group = "Chromia"
            task.description = "Generate Docs Site"
            task.config.set(extension.config)
            task.setChromiaConfig(externalRepoTask.get())
            task.dependsOn(installTask, externalRepoTask)
        }

        val stubsTask = project.tasks.register("rellGenerateClientStubs", RellGenTask::class.java) { task ->
            task.group = "Chromia"
            task.description = "Generate Kotlin Client Stubs"
            task.modules.set(extension.client.modules)
            task.packageName.set(extension.client.packageName)
            task.setChromiaConfig(externalRepoTask.get())
            task.dependsOn(installTask, externalRepoTask)
        }

        project.afterEvaluate {
            val sourceSets = project.extensions.findByName("sourceSets") as? SourceSetContainer
            val mainSourceSet = sourceSets?.getByName("main")
            val kotlinSourceSet = mainSourceSet?.extensions?.getByName("kotlin") as? SourceDirectorySet
            kotlinSourceSet?.srcDir(stubsTask.flatMap { it.output })

            project.tasks.findByName("compileKotlin")?.dependsOn(stubsTask)
        }
    }

    private fun ChromiaTask.setChromiaConfig(remoteTask: RellRemoteConfigTask) {
        if (remoteTask.repository.isPresent) {
            config.set(remoteTask.output.file(remoteTask.configPath.get()))
        } else {
            config.convention { File(project.projectDir, "chromia.yml") }
        }
    }
}
