package com.chromia.rell.gradle.plugin.tasks

import com.chromia.rell.dokka.RellDokkaGenerator
import com.chromia.rell.dokka.config.RellDokkaPluginConfigurationBuilder
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import java.net.URI
import java.nio.file.Path

abstract class RellDocsTask : ChromiaTask() {

    @get:InputDirectory
    val source by lazy { model.compile.source }

    @get:OutputDirectory
    val target: Path by lazy { model.compile.target.resolve("site") }

    @TaskAction
    fun build() {
        RellDokkaPluginConfigurationBuilder(
            title = model.docs.title,
            modules = model.blockchains.values.map { it.module!! },
            projectRoot = source.toFile()
        )
            .customStyleSheets(model.docs.customStyleSheets)
            .customAssets(model.docs.customAssets)
            .includes(model.docs.additionalContentFiles)
            .footerMessage(model.docs.footerMessage)
            .targetFolder(target.toFile())
            .apply {
                model.docs.sourceLink?.let {
                    val localDirectory = model.compile.source
                    addSourceLink(localDirectory.toString(), URI(it.remoteUrl).toURL(), it.remoteLineSuffix)
                }
            }
            .let { RellDokkaGenerator(it).generate() }
    }
}
