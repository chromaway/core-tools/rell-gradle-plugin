package com.chromia.rell.gradle.plugin

import assertk.assertThat
import assertk.assertions.contains
import assertk.assertions.exists
import assertk.assertions.isEqualTo
import org.gradle.testkit.runner.TaskOutcome
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File


/**
 * This functional test tests all features as a whole, to make sure that the features works well together.
 * They are placed in a single test since the [GradleRunner] forks a java process and does not aggregate the coverage data properly.
 * WHen this is resolved it may be split into several test cases.
 */
class RellPluginFunctionalTest {

    @TempDir
    lateinit var projectDir: File


    @Test
    fun testTask() {
        with(File(projectDir, "src/main/rell/main.rell")) {
            parentFile.mkdirs()
            writeText("module; query get_foo() = 1;")
        }
        with(File(projectDir, "src/main/rell/test.rell")) {
            parentFile.mkdirs()
            writeText("@test module; function test_foo() {}")
        }
        with(File(projectDir, "src/main/rell/main_test.rell")) {
            parentFile.mkdirs()
            writeText("@test module; import main.*; function test_bar() {}")
        }
        with(File(projectDir, "chromia.yml")) {
            writeText("""
                blockchains:
                  foo:
                    module: main
                    test:
                      modules:
                        - main_test
                test:
                  modules:
                    - test
                compile:
                  source: src/main/rell
                libs:
                  ft4:
                    registry: https://bitbucket.org/chromawallet/ft3-lib
                    path: rell/src/lib/ft4
                    tagOrBranch: v0.8.0r
                    rid: x"B6AE6AC82AC735BFB9E4E412FFB76BF95380E94F371F5F6A14E71A3AA7D5FEF6"
                    insecure: false
            """.trimIndent())
        }
        with(File(projectDir, "build.gradle")) {
            writeText(
                """
            plugins {
                id 'com.chromia.rell'
            }
            
            chromia {
                config = file("chromia.yml")
                build {
                    blockchains = ["foo"]
                }
                test {
                    useDb = false
                }
                client {
                    modules = ["main"]
                    packageName = "com.example"
                }
            }
        """
            )
        }
        val runner = gradleRunner
            .withProjectDir(projectDir)
            .withPluginClasspath()
            .withDebug(true)
            .withArguments("rellBuild", "rellTest", "rellDocs", "rellGenerateClientStubs")
        val result = runner.build()
        val result2 = runner.build()

        assertThat(result.task(":rellInstall")?.outcome).isEqualTo(TaskOutcome.SUCCESS)
        assertThat(File(projectDir, "src/main/rell/lib/ft4")).exists()
        assertThat(result2.task(":rellInstall")?.outcome).isEqualTo(TaskOutcome.UP_TO_DATE)

        assertThat(result.task(":rellBuild")?.outcome).isEqualTo(TaskOutcome.SUCCESS)
        assertThat(File(projectDir, "build/foo.xml")).exists()
        assertThat(result2.task(":rellBuild")?.outcome).isEqualTo(TaskOutcome.UP_TO_DATE)

        assertThat(result.task(":rellTest")?.outcome).isEqualTo(TaskOutcome.SUCCESS)
        val unitTestReport = File(projectDir, "build/reports/rell/rell-unit-tests.xml")
        assertThat(unitTestReport).exists()
        assertThat(unitTestReport.readText()).contains("tests=\"1\"")
        val bcTestReport = File(projectDir, "build/reports/rell/rell-blockchain-foo-tests.xml")
        assertThat(bcTestReport).exists()
        assertThat(bcTestReport.readText()).contains("tests=\"1\"")
        assertThat(result2.task(":rellTest")?.outcome).isEqualTo(TaskOutcome.UP_TO_DATE)

        assertThat(result.task(":rellDocs")?.outcome).isEqualTo(TaskOutcome.SUCCESS)
        assertThat(File(projectDir, "build/site/index.html")).exists()
        assertThat(result2.task(":rellDocs")?.outcome).isEqualTo(TaskOutcome.UP_TO_DATE)

        assertThat(result.task(":rellGenerateClientStubs")?.outcome).isEqualTo(TaskOutcome.SUCCESS)
        assertThat(File(projectDir, "build/generated-sources/client/main/main.kt")).exists()
        assertThat(result2.task(":rellGenerateClientStubs")?.outcome).isEqualTo(TaskOutcome.UP_TO_DATE)
    }
}
