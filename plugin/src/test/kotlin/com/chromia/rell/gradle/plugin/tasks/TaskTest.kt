package com.chromia.rell.gradle.plugin.tasks

import assertk.assertThat
import assertk.assertions.isInstanceOf
import org.gradle.testfixtures.ProjectBuilder
import org.junit.jupiter.api.Test


class TaskTest {

    @Test
    fun `Rell Plugin adds the correct tasks`() {
        val project = ProjectBuilder.builder().build()
        project.pluginManager.apply("com.chromia.rell")
        assertThat(project.tasks.getByName("rellBuild")).isInstanceOf<RellBuildTask>()
        assertThat(project.tasks.getByName("rellTest")).isInstanceOf<RellTestTask>()
        assertThat(project.tasks.getByName("rellInstall")).isInstanceOf<RellInstallTask>()
        assertThat(project.tasks.getByName("rellDocs")).isInstanceOf<RellDocsTask>()
        assertThat(project.tasks.getByName("rellGenerateClientStubs")).isInstanceOf<RellGenTask>()
    }
}
