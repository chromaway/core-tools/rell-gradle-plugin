# Rell Gradle plugin

[![Kotlin Beta](https://kotl.in/badges/beta.svg)](https://kotlinlang.org/docs/components-stability.html)
[![Build Status](https://gitlab.com/chromaway/core-tools/rell-gradle-plugin/badges/dev/pipeline.svg)](https://gitlab.com/chromaway/core-tools/rell-gradle-plugin/)
[![Coverage Status](https://gitlab.com/chromaway/core-tools/rell-gradle-plugin/badges/dev/coverage.svg)](https://gitlab.com/core-tools/rell-gradle-plugin/)

The rell gradle plugin lets you compile rell code, generate docs or client stubs, from a local or remote repository.

## Example

```kotlin
// settings.gradle(.kts)
pluginManagement {
    repositories {
        maven("https://gitlab.com/api/v4/projects/37675750/packages/maven") { name = "Gradle plugin" }
    }
}
```

### Run tests for a specific chain only

```kotlin
// build.gradle(.kts)
plugin {
    id("com.chromia.rell") version "0.3.1"
}

chromia {
    config = file("chromia.yml") // Default path
    test {
        blockchains = listOf("my-chain")
    }
}
```

### Generate client stubs from a remote repository
```kotlin
// build.gradle(.kts)
plugins {
    id("com.chromia.rell") version "0.3.1"
}

chromia {
    remote {
        repositoryUrl = "https://gitlab.com/chromaway/core/directory-chain"
        tagOrBranch = "1.51.6"
    }
    client {
        packageName = "com.chromia"
        modules = listOf("version")
    }
}
```
